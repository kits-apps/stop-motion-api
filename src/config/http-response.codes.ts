export const HttpErrorCodes = {
  AUDIO_CONVERTER: {
    code: 1000,
    message: 'error_audio_converter',
  },
  IMAGE_CONVERTER: {
    code: 2000,
    message: 'error_image_converter',
  },
  VIDEO_CREATOR: {
    code: 3000,
    message: 'error_video_creator',
  },
};
