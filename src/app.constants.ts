export const AppConstants = {
  AUDIO_PATH: '/tmp/audio/',
  BASE_PATH: '/tmp/',
  IMAGES_PATH: '/tmp/images/',
  VIDEO_PATH: '/tmp/video/',
};
